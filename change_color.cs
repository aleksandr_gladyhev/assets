﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class change_color : MonoBehaviour {

	public int currentFrame;
	public int multiplier = 2;
	public GameObject Cube;
	
	// Update is called once per frame
	void Update () {
		currentFrame++;
		if (currentFrame >= 200*multiplier){
			//Debug.Log("200");
			Cube.GetComponent<Renderer>().material.color  = Color.red;
		} 
		if (currentFrame >= 400*multiplier){
			//Debug.Log("400");
			Cube.GetComponent<Renderer>().material.color  = Color.green;
		} 
		if (currentFrame >= 600*multiplier) {
			//Debug.Log("600");
			Cube.GetComponent<Renderer>().material.color  = Color.blue;
		}
		if (currentFrame >= 800*multiplier*(0+1)){
			currentFrame = 200*multiplier;
			Cube.GetComponent<Renderer>().material.color  = Color.red;
			//Debug.Log(currentFrame);
		}
	}
}
